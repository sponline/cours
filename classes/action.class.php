<?php

class Action {

    protected $_name;
    protected $_format = 'json';
    protected $_id_name;
    protected $_db;
    protected $_need_token = true;
    protected $_security;
    protected $_user;

    public function __construct(string $name, mysqli $db, string $id_name )
    {
        $this->_name = $name;
        $this->_id_name = $id_name;
        $this->_db = $db;

        $this->_security = new Security( $db );

        if(!$this->_security->check($this)){
            echo json_encode(array("request"=>false,"error"=>"problème de token"));
            die();
        };
    }

    public function get( $parametre1 , $parametre2 = null )
    {
        if($parametre1)
        {
            $sql = "SELECT * FROM ".$this->_name." WHERE ".$this->_id_name." = '".mysqli_real_escape_string($this->_db,$parametre1)."'  ";
        } else
        {
            $sql = "SELECT * FROM ".$this->_name." ";
        }

        $resultat = mysqli_query($this->_db,$sql);
        $this->display( $resultat );

    }

    public function delete(  $id )
    {
        if($id)
        {
            $sql = "DELETE FROM ".$this->_name." WHERE ".$this->_id_name." = '".mysqli_real_escape_string($this->_db,$id)."'  ";
            $resultat = mysqli_query($this->_db,$sql);
        }

    }

    public function post(  )
    {
        $body = json_decode(file_get_contents("php://input"));

        $sql_champs = "";
        $sql_value = "";

        foreach($body as $key => $value)
        {
            $sql_champs = $sql_champs . $key.",";
            $sql_value .= '"'.mysqli_real_escape_string($this->_db,$value).'",';
        }

        $sql_champs = substr($sql_champs,0,-1);
        $sql_value = substr($sql_value,0,-1);

        $sql = "INSERT INTO ".$this->_name."(".$sql_champs.") VALUES(".$sql_value.") ";


        $resultat = mysqli_query($this->_db,$sql);

        $this->get( $this->_db->insert_id );

    }

    public function put( $id )
    {
        $body = json_decode(file_get_contents("php://input"));

        $sql_champs_value = "";
        foreach($body as $key => $value)
        {
            $sql_champs_value .= $key.' = "'.mysqli_real_escape_string($this->_db,$value).'",';
        }

        $sql_champs_value = substr($sql_champs_value,0,-1);

        $sql = "UPDATE ".$this->_name." SET ".$sql_champs_value." WHERE ".$this->_id_name.' = "'.mysqli_real_escape_string($this->_db,$id).'" ';

        $resultat = mysqli_query($this->_db,$sql);

        $this->get( $id );


    }

    public function display(  $request  )
    {

        $data = array();
        while($d = $request->fetch_assoc())
        {
            $data[] = $d;
        }

        switch($this->_format)
        {
            case 'json':
                header('Content-Type: application/json');
                echo json_encode( $data );
                break;

        }

    }

    protected function _callAPI($method, $url, $data, $key){
        $curl = curl_init();

        switch ($method){
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            default:
                if ($data) $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        // OPTIONS:


        curl_setopt($curl, CURLOPT_URL, $url);
        if($key) curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: '.$key,
            'Content-Type: application/json',
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        if($key) curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);


        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        curl_close($curl);
        return $result;
    }

    public function needToken()
    {
        return $this->_need_token;
    }

    public function setUser( $user )
    {
        $this->_user = $user;
    }

    public function getUser(  )
    {
        return $this->_user;
    }


}