<?php

class Check extends Action
{

    public function __construct( mysqli $db )
    {
        parent::__construct("itineraires", $db,"Iti_Id");
    }

    public function get($parametre1, $parametre2 = null)
    {
        if($parametre1 === "@me" && $parametre2  )
        {
            $sql = "SELECT * FROM ".$this->_name." WHERE User_Id = '".mysqli_real_escape_string($this->_db,$this->getUser())."' AND ".$this->_id_name." = '".mysqli_real_escape_string($this->_db,$parametre2)."'  ";
            $resultat = $this->_db->query($sql);

            if($resultat && mysqli_num_rows($resultat) == 1)
            {
                $data = array();



                $itineraire = $resultat->fetch_array();
                $duree_trajet = 0;

                $data["info"] = $itineraire;

                require_once __DIR__.'/jour.class.php';
                $jour = new Jour($this->_db);
                $jourData = $jour->gestionData("@me",$parametre2);
                $duree_trajet += $jourData["impact"];

                $data["duree"] = $duree_trajet;

                echo json_encode($data);


            }
        }
    }

    public function post()
    {
        echo json_encode(array("request"=>false,"error"=>"méthode non authorisée"));
    }

    public function delete(int $id)
    {
        echo json_encode(array("request"=>false,"error"=>"méthode non authorisée"));
    }

    public function put($id)
    {
        echo json_encode(array("request"=>false,"error"=>"méthode non authorisée"));
    }


}