<?php

class Travaux extends Action
{

    public function __construct( mysqli $db )
    {
        parent::__construct("travaux", $db,"Tra_Id");
    }

    public function get($parametre1, $parametre2)
    {
        if($parametre1 == "@me" && $parametre2)
        {
            $this->getSpecific($parametre2);
        } else if(!$parametre1 && !$parametre2)
        {
            $this->getAll();
        }
    }

    public function getAll()
    {
        $url = "http://travaux.data.rennesmetropole.fr/api/roadworks";

        $json = json_decode(file_get_contents($url));


        echo json_encode($json);
    }



    public function getSpecific($id)
    {
         $sql = "SELECT * FROM itineraires WHERE User_Id = '".$this->getUser()."' AND Iti_Id = '".mysqli_real_escape_string($this->_db,$id)."' ";

         $resultat = mysqli_query($sql);

         $itineraire = $resultat->fetch_array();

            $url = "http://travaux.data.rennesmetropole.fr/api/roadworks";

            $json = json_decode(file_get_contents($url));


            echo json_encode($json);

    }


}