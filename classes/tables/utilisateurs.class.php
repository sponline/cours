<?php

class Utilisateurs extends Action
{

    public function __construct( mysqli $db )
    {
        parent::__construct("utilisateurs", $db,"User_Id");
    }


    public function post(  )
    {
        $body = json_decode(file_get_contents("php://input"));

        $sql_champs = "";
        $sql_value = "";
        foreach($body as $key => $value)
        {
            if($key == "User_Password") $value = md5($value);
            $sql_champs .= $key.",";
            $sql_value .= '"'.mysqli_real_escape_string($this->_db,$value).'",';
        }

        $sql_champs = substr($sql_champs,0,-1);
        $sql_value = substr($sql_value,0,-1);

        $sql = "INSERT INTO ".$this->_name."(".$sql_champs.") VALUES(".$sql_value.") ";


        $resultat = mysqli_query($this->_db,$sql);

        $this->get( $this->_db->insert_id );


    }

}