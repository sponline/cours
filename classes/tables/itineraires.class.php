<?php

class Itineraires extends Action
{

    private $_clefApi = "8b0eb795b3b20b24f3aa9552994eff2108da4da70e9e3226e55725c2";

    public function __construct( mysqli $db )
    {
        parent::__construct("itineraires", $db,"Iti_Id");
    }

    public function get($id)
    {



        $url = "https://data.explore.star.fr/api/records/1.0/search/?dataset=tco-busmetro-trafic-alertes-tr&facet=niveau&facet=debutvalidite&facet=finvalidite&facet=idligne&facet=nomcourtligne&apikey=".$this->_clefApi;

        $context=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );


        $json = json_decode(file_get_contents($url,false, stream_context_create($context)));

        // Path of the ressource
        $records = $json->{'records'};

        header('Content-Type: application/json');

        echo json_encode($records);
    }


}