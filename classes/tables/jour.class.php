<?php

class Jour extends Action
{

    public function __construct(  $db )
    {
        parent::__construct("itineraires", $db,"Iti_Id");
    }

    public function get($id, $param = null)
    {

        echo json_encode( $this->gestionData($id,$param) );

    }

    public function gestionData($id,$param)
    {

        if($id !== "@me") return false;

        $url = "https://jours-feries-france.antoine-augusti.fr/api/2019";



        $context=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );

        $json = json_decode(file_get_contents($url,false, stream_context_create($context)));

        header('Content-Type: application/json');

        $data = array();
        $data["impact"] = 0;

        if($param) {

            $sql = "SELECT * FROM itineraires WHERE User_Id = '".$this->getUser()."' AND Iti_Id = '".mysqli_real_escape_string($this->_db,$param)."' ";
            $resultat = $this->_db->query($sql);

            if(mysqli_num_rows($resultat) == 1)
            {
                $itineraire = $resultat->fetch_assoc();

                foreach ($json as $jour) {

                    if ($jour->date == $itineraire["Iti_Date"]) {
                        $data["impact"] = 5;
                    }
                }
            }

        }

        return $data;

    }

    public function delete(int $id)
    {
        echo json_encode(array("request"=>false,"error"=>"méthode non authorisée"));
    }


}