<?php

class Security
{
    private $_ok = true;
    private $_action;
    private $_token;
    private $_secret = "LaClefSuperQuiDevraitSecuriser@@!QuiDevraitSecuriserAPIRes@";
    private $_algo = "HS256";
    private $_algo_hmac = "sha256";
    private $_dbb;

    public function __construct(  $db )
    {
        $this->_dbb = $db;
    }

    public  function check( Action $table )
    {
        $this->_action = $table;
        $headers = apache_request_headers();


        if($table->needToken())
        {
            if(!isset($headers['Authorization']) )
            {
                $this->_ok = false;

            }  else
            {
                if( !$this->checkJWT($headers['Authorization']))
                {
                    $this->_ok = false;
                }
            }
        }
        return $this->_ok;
    }



    public function createJWT( int $user_id ): string
    {
        $header = json_encode(['typ' => 'JWT', 'alg' => $this->_algo]);

        $payload = json_encode(
            [
                //"iss" => urlClient,
                "iat" => time(),
                "exp" => time() + ( 24 * 60 * 60 ),
                //"time" => time(),
                //"aud" => urlServeur,
                //"role" => "user",
                'sub' => $user_id
            ]);

        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));

        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));

        $signature = hash_hmac($this->_algo_hmac, $base64UrlHeader . "." . $base64UrlPayload, $this->_secret, true);

        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        return  $base64UrlHeader . "." . $base64UrlPayload . "." . $base64UrlSignature;

    }




    /**
     * @param string $jwt
     * @return bool
     */
    public function checkJWT(string $jwt): bool
    {

        $jwt = str_replace("Bearer ", "",$jwt);
        list($headerEncoded, $payloadEncoded, $signatureEncoded) = explode('.', $jwt);


        $payload = json_decode($this->_base64UrlDecode($payloadEncoded));


        if($payload->exp < time() )
        {

            return false;
        }


        $dataEncoded = "$headerEncoded.$payloadEncoded";

        $signature = $this->_base64UrlDecode($signatureEncoded);

        $rawSignature = hash_hmac($this->_algo_hmac, $dataEncoded, $this->_secret, true);

        if(!hash_equals($rawSignature, $signature)) return false;

        if(!$payload->sub)
        {
            return false;
        } else
        {

            $sql = "SELECT * FROM utilisateurs WHERE User_Id = '".mysqli_real_escape_string($this->_dbb,$payload->sub)."' ";
            $resultat = $this->_dbb->query($sql);


            if ($resultat &&  mysqli_num_rows($resultat) === 1) {
                $this->_action->setUser($payload->sub);
                return true;
            } else {

                return false;
            }

        }




    }



    private function _base64UrlEncode(string $data): string
    {
        $urlSafeData = strtr(base64_encode($data), '+/', '-_');

        return rtrim($urlSafeData, '=');
    }

    private function _base64UrlDecode(string $data): string
    {
        $urlUnsafeData = strtr($data, '-_', '+/');

        $paddedData = str_pad($urlUnsafeData, strlen($data) % 4, '=', STR_PAD_RIGHT);

        return base64_decode($paddedData);
    }


}