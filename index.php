<?php
header('Content-Type: application/json');

include __DIR__."/classes/action.class.php";
include __DIR__."/classes/security.class.php";

$bdd = mysqli_connect("localhost", "testApi",  "testApi","testApi" );

$data = array();


list($chemin,$parametre,$parametre2) = array_pad(explode('/', $_GET["url"]),3, null);

//chargement de la classe en fonction de l'uri (ressources demandées)
$tableFileClass = __DIR__ . "/classes/tables/" . $chemin . ".class.php";
if (file_exists($tableFileClass)) {
    require_once $tableFileClass;
}


$action = new $chemin( $bdd );


    switch ($_SERVER['REQUEST_METHOD']) {
        case "GET" :
            $action->get($parametre,$parametre2);
            break;

        case "DELETE" :
            $action->delete($parametre);
            break;

        case "PUT" :
            $action->put($parametre);
            break;

        case "POST" :
            $action->post();
            break;

}

mysqli_close($bdd);